The compiler jar is ./target/cstr_compiler-1.0.one-jar.jar
It takes one argument, of the input cstr file (.c).

I have not totally completed the implementation of strings, there are some bugs with the string compare operations.
Also I realized that x86_64 does not just use the stack when calling parameters for functions.  It uses rdi, rsi, rdx,
rcx, r8, r9 and then the stack.  I do not use this convention so if you compile helper files with gcc and try to link with
a file compiled with my compiler you might get the wrong answer.  It should be correct for functions with only 1 arguments.
printf will work if you just pass a string.

For testing I implemented a pre-hook (./test/pre-commit.py) which compiles the test files
in ./test/test_cases and runs the binary on inux1.cs.uchicago.edu.
It will compare the return int to the expected output in test.Config.xml.


I did not have time to create a bash file so you will need to use the following to run:

java -jar ./target/cstr_compiler-1.0.one-jar.jar test.c
 
 Note: currently I just delete the macros.  SO you may need to use gcc to replace, sorry.
 