grammar Cstr;

program
    : external_declaration
    | program external_declaration
    ;

external_declaration
    : declaration
    | EXTERN declaration
    | function_definition
    ;

function_definition
    : type function_declarator decl_glb_fct compound_instruction
    ;

decl_glb_fct : ;

declaration
    : type declarator_list ';'
    ;

type
    : INT
    | DOUBLE
    | STRING
    ;

declarator_list
    : declarator
    | declarator_list ',' declarator
    ;

declaration_list
    : declaration
    | declaration_list declaration
    ;

declarator
    : IDENT
    | function_declarator
    ;

function_declarator
    : IDENT '(' ')'
    | IDENT '(' parameter_list ')'
    ;

parameter_list
    : parameter_declaration
    | parameter_list ',' parameter_declaration
    ;

parameter_declaration
    : type IDENT
    ;

instruction
    : ';'
    | compound_instruction
    | expression_instruction
    | iteration_instruction
    | select_instruction
    | jump_instruction
    ;

expression_instruction
    : expression ';'
    | assignment ';'
    ;


assignment
    : IDENT ASSIGNMENT expression
    | parameter_declaration ASSIGNMENT expression
    ;

compound_instruction
    : block_start declaration_list instruction_list block_end
    | block_start declaration_list block_end
    | block_start instruction_list block_end
    | block_start block_end
    ;

block_start : '{' ;

block_end : '}';

instruction_list
    : instruction
    | instruction_list instruction
    ;

select_instruction
    : cond_instruction instruction
    | cond_instruction instruction ELSE instruction
    ;

cond_instruction
    : IF '(' condition ')'
    ;

iteration_instruction
    : WHILE '(' condition ')' instruction
    | DO instruction WHILE '(' condition ')'
    | FOR '(' assignment ';' condition ';' assignment ')' instruction
    ;

jump_instruction
    : RETURN expression ';'
    ;

condition
    : expression comparison_operator expression
    ;

comparison_operator
    :
    | DIFF
    | INF
    | SUP
    | INFEQUAL
    | SUPEQUAL
    | EGAL
    ;

expression
    : expression_additive
    | expression SHIFTLEFT expression_additive
    | expression SHIFTRIGHT expression_additive
    ;

expression_additive
    : expression_multiplicative
    | expression_additive PLUS expression_multiplicative
    | expression_additive MINUS expression_multiplicative
    ;

expression_multiplicative
    : unary_expression
    | expression_multiplicative MULTI unary_expression
    | expression_multiplicative DIV unary_expression
    | expression_multiplicative MODULO unary_expression
    ;

unary_expression
    : postfix_expression
    | MINUS unary_expression
    ;

postfix_expression
    : primary_expression
    | IDENT '(' argument_expression_list')'
    | IDENT '(' ')'
    ;

argument_expression_list
    : expression
    | argument_expression_list',' expression
    ;

primary_expression
    : IDENT
    | CONST_INT
    | CONST_STRING
    | '(' expression ')';


INT :'int';
STRING : 'string';
EXTERN : 'extern';
RETURN : 'return';
IF : 'if';
ELSE : 'else';
WHILE : 'while';
DO : 'do';
FOR : 'for';
THEN : 'then';
PLUS : '+';
MINUS : '-';
MULTI : '*';
DIV : '/';
MODULO : '%';
ASSIGNMENT : '=';
EGAL : '==';
DIFF : '!=';
SUP : '>';
SUPEQUAL : '>=';
INF : '<';
INFEQUAL : '<=';
SHIFTLEFT : '<<';
SHIFTRIGHT : '>>';
IDENT: [a-z][a-z0-9]*;
CONST_INT : [0-9]+;
CONST_DOUBLE : [0-9]*'.'[0-9]*;
CONST_STRING : '"'.*?'"';


//The following are ignoerd in CSTR
WHITESPACE
    :   [ \t\n] -> skip
    ;


BLOCK_COMMENT
    :   '/*'.*?'*/' -> skip
    ;

LINE_COMMENT
    :   '//'~[\r\n]* -> skip
    ;


