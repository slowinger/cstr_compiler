package CstrTypes;

import org.antlr.symtab.Type;

/**
 * Created by slowinger on 1/26/17.
 */
public class CstrInteger extends CstrType implements Type {
    public CstrInteger() {
        this.length = 8;
    }

    public String getName() {
        return "int";
    }

    public int getTypeIndex() {
        return 0;
    }
}
