package CstrTypes;

import org.antlr.symtab.Type;

/**
 * Created by slowinger on 1/26/17.
 */
public class CstrString extends CstrType implements Type {
    public CstrString() {
        this.length = 256;
    }

    public String getName() {
        return "string";
    }

    public int getTypeIndex() {
        return 1;
    }
}
