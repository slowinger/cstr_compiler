import CstrTypes.CstrInteger;
import CstrTypes.CstrString;
import CstrTypes.CstrType;
import org.antlr.symtab.*;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by slowinger on 2/16/17.
 */
public class X86Listener extends CstrBaseListener {

    private int baseOffset;
    private boolean functionBlock, programEntered;
    private String currentJump, currentOut;
    private Scope currentScope;
    private CstrInteger cstrInteger;
    private CstrString cstrString;
    private X86Generator generator;
    private FunctionSymbol currentFunction;
    private ArrayList<String> strings;

    public X86Listener(String outputFile) {

        GlobalScope glob = new GlobalScope(null);

        this.functionBlock = false;
        this.programEntered = false;
        this.baseOffset = 0;
        this.currentScope = glob;
        this.strings = new ArrayList<String>();
        this.cstrInteger = new CstrInteger();
        this.cstrString = new CstrString();
        this.generator = new X86Generator(outputFile);
    }

    @Override
    public void enterProgram(CstrParser.ProgramContext ctx) {

        if (!programEntered) {
            this.programEntered = true;
            visitProgram(ctx);
            generator.programFooter(strings);
        }
    }

    private void visitProgram(CstrParser.ProgramContext ctx) {

        if (ctx.program() != null)
            visitProgram(ctx.program());

        visitExternal_declaration(ctx.external_declaration());
    }

    private void visitExternal_declaration(CstrParser.External_declarationContext ctx) {

        ///EXTERN is ignoerd?
        if (ctx.declaration() != null)
            visitDeclaration(ctx.declaration());
        else
            visitFunctionDefinition(ctx.function_definition());
    }

    private void visitFunctionDefinition(CstrParser.Function_definitionContext ctx) {

        ///ignoring decl_glb_fct
        createFunction(ctx.function_declarator(), getTypeFromContext(ctx.type()), true);
        this.functionBlock = true;
        visitFunctionCompoundInstruction(ctx.compound_instruction(), this.currentFunction);
    }

    private void visitInstructionList(CstrParser.Instruction_listContext ctx) {

        if (ctx.instruction_list() != null) {
            visitInstructionList(ctx.instruction_list());
        }
        visitInstruction(ctx.instruction());
    }

    private void visitExpressionInstruction(CstrParser.Expression_instructionContext ctx) {

        if (ctx.expression() != null) {
            visitExpression(ctx.expression());
        } else {
            visitAssignment(ctx.assignment());
        }
    }

    private void visitAssignment(CstrParser.AssignmentContext ctx) {

        int location;
        Type type;
        if (ctx.IDENT() != null) {

            Symbol symbol = getSymbol(ctx.IDENT().getText());
            if (symbol != null) {
                visitExpression(ctx.expression());
                location = ((Variable)symbol).getLocation();
                ((Variable) symbol).initialized = true;
                type = ((Variable) symbol).getType();
            } else {
                throw new UndeclaredVariableException();
            }
        }
        else {

            String variableName = ctx.parameter_declaration().IDENT().getText();
            type = getTypeFromContext(ctx.parameter_declaration().type());
            createVariable(variableName, type);
            visitExpression(ctx.expression());
            location = ((Variable)getSymbol(variableName)).getLocation();
        }
        generator.pushVariable(location, type, this.strings.size()-1);
    }

    private Type visitExpression(CstrParser.ExpressionContext ctx) {

        Type type;
        Type typeAdditive;

        if (ctx.SHIFTLEFT() != null) {

            type = visitExpression(ctx.expression());
            typeAdditive = visitExpressionAdditive(ctx.expression_additive());
            generator.shiftLeft();
        } else if (ctx.SHIFTRIGHT() != null) {

            type = visitExpression(ctx.expression());
            typeAdditive = visitExpressionAdditive(ctx.expression_additive());
            generator.shiftRight();
        } else
            return visitExpressionAdditive(ctx.expression_additive());
        if (!compareTypes(type, typeAdditive))
            throw new MismatchedSyntaxException(ctx.expression().getText() + " : " + type.getName() + " does not match "
                    + ctx.expression_additive().getText() + " : " + typeAdditive.getName());
        else
            return type;

    }

    private void visitInstruction(CstrParser.InstructionContext ctx) {

        if (ctx.compound_instruction() != null)
            visitCompoundInstruction(ctx.compound_instruction());
        else if (ctx.expression_instruction() != null)
            visitExpressionInstruction(ctx.expression_instruction());
        else if (ctx.iteration_instruction() != null)
            visitIterationInstruction(ctx.iteration_instruction());
        else if (ctx.select_instruction() != null)
            visitSelectInstruction(ctx.select_instruction());
        else if (ctx.jump_instruction() != null){
            FunctionScope functionScope = (FunctionScope) this.currentScope;

            String function = functionScope.functionName;
            visitJumpInstruction(ctx.jump_instruction(), function, functionScope.wasReturned);
            functionScope.wasReturned = true;

        }
    }

    private void visitIterationInstruction(CstrParser.Iteration_instructionContext ctx) {

        String jump = generator.getNextJumpInstruction();
        String loop = generator.getNextLoop();
        if (ctx.FOR() != null) {

            visitAssignment(ctx.assignment(0));
            generator.handleOutput(loop + ":", false);
            visitCondition(ctx.condition(), jump, false);
            visitInstruction(ctx.instruction());
            visitAssignment(ctx.assignment(1));
            generator.handleOutput("jmp    " + loop, true);
            generator.handleOutput(jump + ":", false);

        } else if (ctx.DO() != null) {

            generator.handleOutput(loop + ":", false);
            visitInstruction(ctx.instruction());
            visitCondition(ctx.condition(), loop, true);
            generator.handleOutput(jump + ":", false);
            
        } else if (ctx.WHILE() != null){

            generator.handleOutput(loop + ":", false);
            visitCondition(ctx.condition(), jump, false);
            visitInstruction(ctx.instruction());
            generator.handleOutput("jmp    " + loop, true);
            generator.handleOutput(jump + ":", false);
        }
    }

    private void visitSelectInstruction(CstrParser.Select_instructionContext ctx) {

        if (ctx.ELSE() != null) {

            visitConditionInstruction(ctx.cond_instruction());
            visitInstruction(ctx.instruction(0));
            generator.handleOutput("jmp    " + this.currentOut, true);
            generator.handleOutput(this.currentJump + ":", false);
            visitInstruction(ctx.instruction(1));
            generator.handleOutput(this.currentOut + ":", false);
        } else {

            visitConditionInstruction(ctx.cond_instruction());
            visitInstruction(ctx.instruction(0));
            generator.handleOutput(this.currentJump + ":", false);
        }
    }

    private void visitConditionInstruction(CstrParser.Cond_instructionContext ctx) {

        this.currentJump = generator.getNextJumpInstruction();
        this.currentOut = generator.getNextOutInstruction();
        visitCondition(ctx.condition(), this.currentJump, false);

    }

    private void visitCondition(CstrParser.ConditionContext ctx, String jumpInstruction, boolean jumpOnTrue) {

        Type compare = visitExpression(ctx.expression(1));
        visitExpression(ctx.expression(0));

        boolean isString = compare.getTypeIndex() == 1;
        evaluateComparisonOperator(ctx.comparison_operator(), jumpInstruction, jumpOnTrue, isString);
    }

    private void evaluateComparisonOperator(CstrParser.Comparison_operatorContext ctx, String jumpInstruction,
                                            boolean jumpOnTrue, boolean isString) {

        if (jumpOnTrue) {

            if (ctx.DIFF() != null)
                generator.comparisonEqual(jumpInstruction, isString);
            else if (ctx.EGAL() != null)
                generator.comparisonDifference(jumpInstruction, isString);
            else if (ctx.INF() != null)
                generator.comparisonGreaterThanEqual(jumpInstruction, isString);
            else if (ctx.SUP() != null)
                generator.comparisonLessThanEqual(jumpInstruction, isString);
            else if (ctx.INFEQUAL() != null)
                generator.comparisonGreaterThan(jumpInstruction, isString);
            else
                generator.comparisonLessThan(jumpInstruction, isString);

        } else {

            if (ctx.DIFF() != null)
                generator.comparisonDifference(jumpInstruction, isString);
            else if (ctx.EGAL() != null)
                generator.comparisonEqual(jumpInstruction, isString);
            else if (ctx.INF() != null)
                generator.comparisonLessThan(jumpInstruction, isString);
            else if (ctx.SUP() != null)
                generator.comparisonGreaterThan(jumpInstruction, isString);
            else if (ctx.INFEQUAL() != null)
                generator.comparisonLessThanEqual(jumpInstruction, isString);
            else
                generator.comparisonGreaterThanEqual(jumpInstruction, isString);
        }

    }

    private void visitJumpInstruction(CstrParser.Jump_instructionContext ctx, String function, boolean wasReturned) {

        visitExpression(ctx.expression());
        generator.functionFooter(function, wasReturned);
    }

    private void visitCompoundInstruction(CstrParser.Compound_instructionContext ctx) {

        LocalScope scope;
        if (this.functionBlock) {

            scope = new FunctionScope(null, this.currentFunction.getName());
            this.functionBlock = false;

        } else
            scope = new LocalScope(null);

        pushScope(scope);

        if (ctx.declaration_list() != null)
            visitDeclarationList(ctx.declaration_list());

        if (ctx.instruction_list() != null)
            visitInstructionList(ctx.instruction_list());

        popScope();
    }

    private void visitFunctionCompoundInstruction(CstrParser.Compound_instructionContext ctx, FunctionSymbol function) {

        FunctionScope scope = new FunctionScope(null, this.currentFunction.getName());
        pushScope(scope);

        for (Symbol symbol: function.getSymbols())
            createParameter(symbol.getName(), ((ParameterSymbol) symbol).getType());


        if (ctx.declaration_list() != null)
            visitDeclarationList(ctx.declaration_list());

        if (ctx.instruction_list() != null)
            visitInstructionList(ctx.instruction_list());

        popScope();
    }

    private void visitDeclaration(CstrParser.DeclarationContext ctx) {

        visitDeclaratorList(ctx.declarator_list(), getTypeFromContext(ctx.type()));
    }

    private void visitDeclarationList(CstrParser.Declaration_listContext ctx) {

        if (ctx.declaration_list() != null)
            visitDeclarationList(ctx.declaration_list());
        visitDeclaration(ctx.declaration());
    }

    private void visitDeclaratorList(CstrParser.Declarator_listContext ctx, Type type) {

        if (ctx.declarator_list() != null)
            visitDeclaratorList(ctx.declarator_list(), type);

        visitDeclarator(ctx.declarator(), type);
    }

    private void visitDeclarator(CstrParser.DeclaratorContext ctx, Type type) {

        if (ctx.IDENT() != null)
            createVariable(ctx.IDENT().getText(), type);
         else {
            createFunction(ctx.function_declarator(), type, false);
        }
    }

    private Type visitExpressionAdditive(CstrParser.Expression_additiveContext ctx) {

        Type typeAdditive;
        Type typeMultiplicative;

        if (ctx.PLUS() != null) {

            typeMultiplicative = visitExpressionMultiplicative(ctx.expression_multiplicative());
            typeAdditive = visitExpressionAdditive(ctx.expression_additive());

            generator.addition(typeAdditive);

        } else if (ctx.MINUS() != null) {
            typeAdditive = visitExpressionAdditive(ctx.expression_additive());
            typeMultiplicative = visitExpressionMultiplicative(ctx.expression_multiplicative());

            if (typeAdditive.getTypeIndex()==1)
                throw new MismatchedSyntaxException("Minus instructions are not valid for strings");

            generator.subtraction();

        } else
            return visitExpressionMultiplicative(ctx.expression_multiplicative());
        if (!compareTypes(typeMultiplicative, typeAdditive))
            throw new MismatchedSyntaxException(ctx.expression_additive().getText() + " : " + typeAdditive.getName() +
                    " does not match " + ctx.expression_multiplicative().getText() + " : " + typeMultiplicative.getName());
        else
            return typeAdditive;


    }

    private Type visitExpressionMultiplicative(CstrParser.Expression_multiplicativeContext ctx) {

        Type typeMultiplicative;
        Type typeUnary;

        if (ctx.MULTI() != null) {
            typeMultiplicative = visitExpressionMultiplicative(ctx.expression_multiplicative());
            typeUnary = visitUnaryExpression(ctx.unary_expression());
            generator.multiplication();
        } else if (ctx.DIV() != null) {
            typeMultiplicative = visitExpressionMultiplicative(ctx.expression_multiplicative());
            typeUnary = visitUnaryExpression(ctx.unary_expression());
            generator.division();
        } else if (ctx.MODULO() != null) {
            typeMultiplicative = visitExpressionMultiplicative(ctx.expression_multiplicative());
            typeUnary = visitUnaryExpression(ctx.unary_expression());
            generator.modulo();
        } else
            return visitUnaryExpression(ctx.unary_expression());

        if (typeMultiplicative.getTypeIndex() == 1)
            throw new MismatchedSyntaxException("Multiplication instructions are not valid for strings");

        if (!compareTypes(typeUnary, typeMultiplicative))
            throw new MismatchedSyntaxException(ctx.expression_multiplicative().getText() + " : " +
                    typeMultiplicative.getName() + " does not match " +
                    ctx.unary_expression().getText() + " : " + typeUnary.getName());
        else
            return typeMultiplicative;

    }

    private Type visitUnaryExpression(CstrParser.Unary_expressionContext ctx) {

        if (ctx.MINUS() != null) {
            Type type = visitUnaryExpression(ctx.unary_expression());
            if (type.getTypeIndex() != 1) {
                generator.negate();
                return type;
            } else
                throw new MismatchedSyntaxException("Negation is not a valid instruction for strings");
        } else
            return visitPostftixExpression(ctx.postfix_expression());
    }

    private Type visitPostftixExpression(CstrParser.Postfix_expressionContext ctx) {

        if (ctx.IDENT() != null) {
            return makeFunctionCall(ctx);
        } else
            return visitPrimaryExpression(ctx.primary_expression());
    }

    private Type makeFunctionCall(CstrParser.Postfix_expressionContext ctx) {

        ArrayList<Type> arguments = new ArrayList<Type>();

        if (ctx.argument_expression_list() != null)
            arguments = visitFunctionArguments(ctx.argument_expression_list());
        FunctionSymbol function = getFunction(ctx.IDENT().getText(), arguments);
        if (function != null) {
            boolean isString = false;
            if (arguments.size() >= 1 && arguments.get(arguments.size()-1).getTypeIndex()==1)
                isString = true;
            generator.callFunction(ctx.IDENT().getText(), isString);
        }

         else
            throw new UndeclaredFunctionException("function " + ctx.IDENT().getText() + " was not declared");

        return function.getType();
    }

    private ArrayList<Type> visitFunctionArguments(CstrParser.Argument_expression_listContext ctx) {

        ArrayList<Type> typesOut;
        Type type = visitExpression(ctx.expression());

        if (ctx.argument_expression_list() != null)
            typesOut = visitFunctionArguments(ctx.argument_expression_list());
         else
            typesOut = new ArrayList<Type>();

        typesOut.add(type);
        return typesOut;
    }

    private Type  visitPrimaryExpression(CstrParser.Primary_expressionContext ctx) {

        if (ctx.IDENT() != null) {
            Symbol symbol = getSymbol(ctx.IDENT().getText());
            if (symbol != null) {

                if (!((Variable)symbol).functionParameter) {
                    if (!((Variable)symbol).initialized)
                        throw new UndeclaredVariableException("Variable \"" + symbol.getName() + "\" has not been initialized");
                }

                int location =  ((Variable)symbol).getLocation();

                generator.popVariable(location, ((Variable) symbol).getType());
                generator.push();

                return ((Variable) symbol).getType();
            } else {
                visitErrorNode(ctx);
                throw new UndeclaredVariableException();
            }

        } else if (ctx.CONST_INT() != null) {
            generator.popConstant(Integer.parseInt(ctx.CONST_INT().getText()));
            generator.push();
            return this.cstrInteger;
        } else if (ctx.CONST_STRING() != null) {
            createString(ctx.CONST_STRING().getText());
            generator.pushString(strings.size()-1);
            return this.cstrString;
        } else
            return visitExpression(ctx.expression());
    }

    private Type getTypeFromContext(CstrParser.TypeContext ctx) {

        if (ctx.INT() != null)
            return this.cstrInteger;
         else if (ctx.STRING() != null)
            return this.cstrString;
         else
            throw new InvalidCompilerInputException();

    }

    private boolean compareTypes(Type type1, Type type2) {

        return  type1.getTypeIndex() == type2.getTypeIndex();
    }

    private void createVariable(String name, Type type) {

        Variable variable = new Variable(name, getNextOffset(type));
        variable.setType(type);
        this.currentScope.define(variable);

        generator.instantiateVariable(type);

    }

    private int createString(String string) {

        strings.add(string);
        return strings.size() -1;
    }

    private void createParameter(String name, Type type) {

        int offset = ((FunctionScope) this.currentScope).getNextOffset();
        Variable variable = new Variable(name, offset);
        variable.setType(type);
        variable.functionParameter = true;
        this.currentScope.define(variable);

    }

    private void createFunction(CstrParser.Function_declaratorContext ctx, Type type, boolean declare) {

        FunctionSymbol function = new FunctionSymbol(ctx.IDENT().getText());
        function.setType(type);
        this.currentFunction = function;

        if (ctx.parameter_list() != null)
            addFunctionParameters(ctx.parameter_list());

        this.currentScope.define(function);
        if (declare)
            generator.declareFunction(function.getName());
    }

    private void addFunctionParameters(CstrParser.Parameter_listContext ctx) {

        if (ctx != null) {

            addFunctionParameters(ctx.parameter_list());
            ParameterSymbol symbol = new ParameterSymbol(ctx.parameter_declaration().IDENT().getText());
            Type type = getTypeFromContext(ctx.parameter_declaration().type());
            symbol.setType(type);
            this.currentFunction.define(symbol);

        }
    }

    private void pushScope(Scope s) {

        s.setEnclosingScope(this.currentScope);
        this.currentScope = s;
    }

    private void popScope() {

        this.currentScope = this.currentScope.getEnclosingScope();
    }

    private Symbol getSymbol(String strSymbol) {

        Scope scope = this.currentScope;
        Symbol symbol;
        while (scope != null) {
            symbol = scope.getSymbol(strSymbol);
            if (symbol != null) {
                return symbol;
            }
            scope = scope.getEnclosingScope();
        }
        return null;
    }

    private FunctionSymbol getFunction(String strFunction, ArrayList<Type> arguments) {

        Scope scope = this.currentScope;
        Symbol function;
        while (scope != null){
            function = scope.getSymbol(strFunction);
            if (function != null && function instanceof FunctionSymbol) {
                if (!matchFunctionParameters((FunctionSymbol) function, arguments))
                    return null;
                return (FunctionSymbol) function;
            }
            scope = scope.getEnclosingScope();
        }
        return null;
    }

    private boolean matchFunctionParameters(FunctionSymbol function, List<Type> arguments) {

        List parameters = function.getSymbols();

        if (parameters.size() != arguments.size())
            return false;

        for (int i = 0; i < parameters.size(); i++) {
            Type parameterType = ((ParameterSymbol) parameters.get(i)).getType();
            Type argumentType = arguments.get(parameters.size() - i - 1);
            if (parameterType.getTypeIndex() != argumentType.getTypeIndex())
                return false;
        }
        return true;
    }

    private int getNextOffset(Type type) {

        if (type.getTypeIndex() == 0)
            this.baseOffset -= 8;
        else if (type.getTypeIndex() == 1)
            this.baseOffset -= 128;

        return this.baseOffset;
    }

    private class Variable extends VariableSymbol {

        int location; //rbp
        boolean initialized;
        boolean functionParameter;

        Variable(String name, int location) {
            super(name);
            this.location = location;
            this.initialized = false;
            this.functionParameter = false;
        }

        int getLocation() {
            return this.location;
        }
    }

    public void visitErrorNode(CstrParser.Primary_expressionContext ctx) {

        System.out.println("ERROR - Variable: \"" + ctx.IDENT().getText() + "\" was not declared");
    }

    class InvalidCompilerInputException extends NullPointerException {
        public InvalidCompilerInputException() {
            super();
        }

        public InvalidCompilerInputException(String message) {
            super(message);
        }
    }

    class UndeclaredVariableException extends InvalidCompilerInputException {

        public UndeclaredVariableException() {
            super();
        }
        public UndeclaredVariableException(String message) {
            super(message);
        }

    }

    class UndeclaredFunctionException extends InvalidCompilerInputException {

        public UndeclaredFunctionException(String message) {
            super(message);
        }

    }

    class MismatchedSyntaxException extends InvalidCompilerInputException {

        public MismatchedSyntaxException(String message) {
            super(message);
        }

    }

    class FunctionScope extends LocalScope {

        String functionName;
        int parameterOffset;
        boolean wasReturned;
        public FunctionScope(Scope enclosingScope, String function) {
            super(enclosingScope);
            this.functionName = function;
            this.parameterOffset = 8;
            this.wasReturned = false;
        }

        int getNextOffset() {
            this.parameterOffset += 8;
            return this.parameterOffset;
        }
    }
}
