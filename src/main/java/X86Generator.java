import org.antlr.symtab.Type;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by slowinger on 2/16/17.
 */
public class X86Generator {
    private int jumpInstruction, loop;
    private boolean initialized;
    private FileWriter fileWriter;
    private BufferedWriter bufferedWriter;

    public X86Generator(String outputFile) {

        this.initialized = false;
        this.jumpInstruction = 0;
        this.loop = 0;

        try {
            fileWriter = new FileWriter(outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        bufferedWriter = new BufferedWriter(fileWriter);
    }

    public void handleOutput(String output, boolean indent) {

        String sIndent = "";
        if (indent) {
            sIndent = "      ";
        }
        try {
            StringBuffer buffer = new StringBuffer(sIndent + output + "\n");
            bufferedWriter.append(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getNextJumpInstruction() {

        return ".no" + this.jumpInstruction++;
    }
    public String getNextOutInstruction() {

        return ".out" + this.jumpInstruction;
    }
    public String getNextLoop() {

        return ".loop" + this.loop++;
    }

    public void popVariable(int location, Type type) {

        if (type.getTypeIndex() == 0) {
            handleOutput("movq    " + location + "(%rbp), %rax", true);
        } else {
            handleOutput("leaq    " + location + "(%rbp), %rax", true);
        }

    }

    public void popConstant(int constant) {

        handleOutput("movq    $" + constant + ", %rax", true);
    }


    public void push() {

        handleOutput("pushq    %rax", true);
    }

    public void pushString(int string) {

        handleOutput("pushq    $.s" + string, true);
    }

    public void skipLine() {

        handleOutput("\n", false);
    }

    public void callFunction(String function, boolean isString) {

        if (!isString)
            handleOutput("movq    %rax, %rdi", true);
        else {
            handleOutput("popq    %rdi", true);
            handleOutput("movq    $0, %rax", true);
        }

        handleOutput("call " + function, true);
        push();
    }

    public void programFooter(ArrayList<String> strings) {

        skipLine();
        handleOutput(".comm .stracc,256,4", true);
        if (strings.size() > 0) {
            handleOutput(".section        .rodata", false);
            for (int i = 0; i < strings.size(); i++) {
                handleOutput(".s" + i + ":    .string " + strings.get(i), true);
            }
        }

        try {
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void declareFunction(String function) {

        functionHeader(function);
        handleOutput(function + ":", false);
        handleOutput("pushq   %rbp", true);
        handleOutput("movq    %rsp, %rbp", true);
    }

    private void functionHeader(String function) {

        skipLine();
        handleOutput(".globl " + function, false);
        handleOutput(".type " + function +",@function", true);
    }

    public void functionFooter(String function, boolean wasReturned) {

        handleOutput("# RETURN_instr", false);
        handleOutput("popq    %rax", true);
        handleOutput("jmp ." + function + "_ret", true);

        if (!wasReturned) {
            handleOutput("." + function + "_ret:", false);
            handleOutput("leave", true);
            handleOutput("ret", true);
        }

    }

    public void negate() {

        handleOutput("# NEG_call_instr", false);
        handleOutput("popq    %rax", true);
        handleOutput("negq    %rax", true);
        push();
    }

    public void shiftLeft() {

        handleOutput("# SHIFT_left_instr", false);
        handleOutput("popq    %rcx", true);
        handleOutput("popq    %rax", true);
        handleOutput("salq   %cl, %rax", true);

        push();
    }

    public void shiftRight() {

        handleOutput("# SHIFT_right_instr", false);
        handleOutput("popq    %rcx", true);
        handleOutput("popq    %rax", true);
        handleOutput("sarq   %cl, %rax", true);

        push();
    }

    public void division() {

        handleOutput("# DIV_arithmetic_instr", false);
        handleOutput("popq    %rcx", true);
        handleOutput("popq    %rax", true);
        handleOutput("cqto", true);
        handleOutput("idivq   %rcx", true);

        push();
    }

    public void modulo() {

        handleOutput("# DIV_arithmetic_instr", false);
        handleOutput("popq    %rcx", true);
        handleOutput("popq    %rax", true);
        handleOutput("cqto", true);
        handleOutput("idivq   %rcx", true);
        handleOutput("movq    %rdx, %rax", true);
        handleOutput("pushq    %rax", true);

    }

    public void multiplication() {

        handleOutput("# MULT_arithmetic_instr", false);
        handleOutput("popq    %rcx", true);
        handleOutput("popq    %rax", true);
        handleOutput("imulq   %rcx, %rax", true);
        push();
    }


    public void addition(Type type) {

        if (type.getTypeIndex()== 0) {
            handleOutput("# PLUS_arithmetic_instr", false);
            handleOutput("popq    %rcx", true);
            handleOutput("popq    %rax", true);
            handleOutput("addq    %rcx, %rax", true);
            push();
        } else if (type.getTypeIndex() == 1) {
            handleOutput("# STRCCAT_instr", false);
            handleOutput("movl    $128,%edx", true);
            handleOutput("popq    %rsi", true);
            handleOutput("movq    $.stracc,%rdi", true);
            handleOutput("call    strncpy", true);
            handleOutput("movb    $0, 127(%rdi)", true);
            handleOutput("popq    %rsi", true);
            handleOutput("call    strcat", true);
            handleOutput("movb    $0, 127(%rdi)", true);
            handleOutput("pushq   %rdi", true);
        }

    }

    public void subtraction() {

        handleOutput("# SUB_arithmetic_instr", false);
        handleOutput("popq    %rcx", true);
        handleOutput("popq    %rax", true);
        handleOutput("subq    %rcx, %rax", true);
        push();
    }


    public void pushVariable(int location, Type type, int stringLocation) {

        handleOutput("# PUSH_variable_instr", false);
        if (type.getTypeIndex() == 0) {
            handleOutput("popq    %rax", true);
            handleOutput("movq    %rax, " + location + "(%rbp)", true);

        } else if (type.getTypeIndex() == 1) {
            handleOutput("movl    $128,%edx", true);
            handleOutput("popq    %rsi", true);
            handleOutput("leaq    "  + location + "(%rbp),%rdi", true);
            handleOutput("call    strncpy", true);
            handleOutput("movb    $0, 127(%rdi)", true);
        }

    }

    public void instantiateVariable(Type type) {

        if (type.getTypeIndex() == 0)
            handleOutput("subq    $8, %rsp", true);
        else if (type.getTypeIndex() == 1)
            handleOutput("subq    $128, %rsp", true);
    }


    //Comparison Operators
    private void comparison(boolean stringCompare) {

        if (stringCompare) {
            handleOutput("popq    %rdi", true);
            handleOutput("popq    %rsi", true);
            handleOutput("movq    $0,%rax", true);
            handleOutput("call    strncmp", true);
            handleOutput("cmpq    $0,%rax", true);
        } else {
            handleOutput("popq    %rax", true);
            handleOutput("popq    %rcx", true);
            handleOutput("cmpq    %rcx, %rax", true);
        }

    }

    public void comparisonDifference(String jump, boolean stringCompare) {

        handleOutput("# COMPARISON_NEqual_instr", false);
        comparison(stringCompare);
        handleOutput("je    " + jump, true);
    }

    public void comparisonEqual(String jump, boolean stringCompare) {

        handleOutput("# COMPARISON_Equal_instr", false);
        comparison(stringCompare);
        handleOutput("jne    " + jump, true);
    }

    public void comparisonLessThan(String jump, boolean stringCompare) {

        handleOutput("# COMPARISON_Less_instr", false);
        comparison(stringCompare);
        handleOutput("jge    " + jump, true);
    }

    public void comparisonGreaterThan(String jump, boolean stringCompare) {

        handleOutput("# COMPARISON_Greater_instr", false);
        comparison(stringCompare);
        handleOutput("jle    " + jump, true);
    }

    public void comparisonLessThanEqual(String jump, boolean stringCompare) {

        handleOutput("# COMPARISON_Less_Equal_instr", false);
        comparison(stringCompare);
        handleOutput("jg    " + jump, true);
    }

    public void comparisonGreaterThanEqual(String jump, boolean stringCompare) {

        handleOutput("# COMPARISON_Greater_Equal_instr", false);
        comparison(stringCompare);
        handleOutput("jl    " + jump, true);
    }

}
