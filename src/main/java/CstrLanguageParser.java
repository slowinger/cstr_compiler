import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

/**
 * Created by slowinger on 1/19/17.
 */
public class CstrLanguageParser {
    private static final String assemblyExtension = ".s";

    public static void main(String[] args) {

        String fileName = args[0];
        File file = new File(fileName);
        FileInputStream fis;

        try {

            fis = new FileInputStream(file);
            ANTLRInputStream input = new ANTLRInputStream(fis);
            CstrLexer lexer = new CstrLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            CstrParser parser = new CstrParser(tokens);
            ParseTree tree = parser.program();
            fis.close();

            ParseTreeWalker walker = new ParseTreeWalker();
            String outputFile = fileName.substring(0,fileName.length()-2) + assemblyExtension;
            X86Listener listener = new X86Listener(outputFile);
            try {
                walker.walk(listener, tree);
                System.out.println("Compilation complete, no errors detected");
            } catch (NullPointerException e) {
                //e.printStackTrace();
                System.out.println(e.getLocalizedMessage());
                System.out.println("ERROR: Unable to compute - See previous error");
            }

        } catch (IOException e) {

            e.printStackTrace();
        }
    }
}
